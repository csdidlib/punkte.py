#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#

def punkteverteilung(x):
    if x >= 6:
        y = x // 2
        z = y // 2
        i = (x - y) / 4
        i = round(i + 0.49)
        c = y + i  
        b = y + (2 * i)
        a = y + (3 * i)
        if x <= 10:
            a = x
        return(x, a, a - 1, b, b - 1, c, c - 1, y, y - 1, z, z - 1, 0)
    else:
        return(0)
