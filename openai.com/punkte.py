def punkteverteilung(x):
    if x < 6:
        return [0]
    
    y = x // 2
    z = y // 2
    i = round((x - y) / 4 + 0.49)
    c = y + i  
    b = y + 2 * i
    a = y + 3 * i if x > 10 else x
    return [x, a, a - 1, b, b - 1, c, c - 1, y, y - 1, z, z - 1, 0]

x = int(input("? "))

if x >= 6:
    points = punkteverteilung(x)
    for j in range(6):
        print(j + 1, ":", points[2*j], "-", points[2*j + 1])
else:
    print(0)