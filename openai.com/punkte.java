import java.util.Scanner;

public class Punkteverteilung {
    public static int[] punkteverteilung(int x) {
        if (x < 6) {
            return new int[]{0};
        }

        int y = x / 2;
        int z = y / 2;
        int i = Math.round((x - y) / 4 + 0.49f);
        int c = y + i;
        int b = y + 2 * i;
        int a = x > 10 ? y + 3 * i : x;

        return new int[]{x, a, a - 1, b, b - 1, c, c - 1, y, y - 1, z, z - 1, 0};
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("? ");
        int x = scanner.nextInt();

        if (x >= 6) {
            int[] points = punkteverteilung(x);
            for (int j = 0; j < 6; j++) {
                System.out.println((j + 1) + " : " + points[2*j] + " - " + points[2*j + 1]);
            }
        } else {
            System.out.println(0);
        }
    }
}
