function punkteverteilung(x) {
    if (x < 6) {
        return [0];
    }

    const y = Math.floor(x / 2);
    const z = Math.floor(y / 2);
    const i = Math.round((x - y) / 4 + 0.49);
    const c = y + i;
    const b = y + 2 * i;
    const a = x > 10 ? y + 3 * i : x;

    return [x, a, a - 1, b, b - 1, c, c - 1, y, y - 1, z, z - 1, 0];
}

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('? ', (input) => {
    const x = parseInt(input);

    if (x >= 6) {
        const points = punkteverteilung(x);
        for (let j = 0; j < 6; j++) {
            console.log((j + 1) + " : " + points[2*j] + " - " + points[2*j + 1]);
        }
    } else {
        console.log(0);
    }

    rl.close();
});
