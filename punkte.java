import java.util.Scanner;

public class Punkteverteilung {

    public static int[] punkteverteilung(int x) {
        if (x >= 6) {
            int y = x / 2;
            int z = y / 2;
            int i = Math.round((x - y) / 4.0f);
            int c = y + i;
            int b = y + (2 * i);
            int a = y + (3 * i);
            if (x <= 10) {
                a = x;
            }
            return new int[]{x, a, a - 1, b, b - 1, c, c - 1, y, y - 1, z, z - 1, 0};
        } else {
            return new int[]{0};
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = 0;
        int j = 1;
        System.out.print("? ");
        int x = Integer.parseInt(scanner.nextLine());

        if (x >= 6) {
            int[] n = punkteverteilung(x);

            while (j <= 6) {
                System.out.println(j + ": " + n[k] + " - " + n[k + 1]);
                j++;
                k += 2;
            }
        } else {
            System.out.println(0);
        }
    }
}