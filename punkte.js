function punkteverteilung(x) {
    if (x >= 6) {
        let y = Math.floor(x / 2);
        let z = Math.floor(y / 2);
        let i = (x - y) / 4;
        i = Math.round(i + 0.49);
        let c = y + i;
        let b = y + (2 * i);
        let a = y + (3 * i);
        if (x <= 10) {
            a = x;
        }
        return [x, a, a - 1, b, b - 1, c, c - 1, y, y - 1, z, z - 1, 0];
    } else {
        return [0];
    }
}

let k = 0;
let j = 1;
let x = parseInt(prompt("? "));

if (x >= 6) {
    let n = punkteverteilung(x);

    while (j <= 6) {
        console.log(j + ":", n[k], "-", n[k + 1]);
        j++;
        k += 2;
    }
} else {
    console.log(0);
}
